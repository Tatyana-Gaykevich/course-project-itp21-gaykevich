﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace CourseWork.ViewModels.Tests
{
    [TestClass()]
    public class LoginViewModelTests
    {
       
        [TestMethod()]
        public void Login_True_Test()
        {
            var ru = Mock.Of<IRepository<User>>(
                r=>r.GetAll()==new List<User>() {
                    new User {
                    Id = 1,
                    Login = "Admin",
                    Password = "11111",
                    RoleId=0} 
                });
            
            
            var rr= Mock.Of<IRepository<Role>>(
                r=>r.GetAll()==new List<Role>()&& 
            r.Get(0)==new Role { Id=0,Name="админ"});
            LoginViewModel lm = new LoginViewModel(ru,rr);
            lm.User.Login = "Admin";
            Assert.IsTrue(lm.Login("11111"));
        }
        [TestMethod()]
        public void Login_False_Test()
        {
            var ru = Mock.Of<IRepository<User>>(
                r => r.GetAll() == new List<User>() {
                    new User {
                    Id = 1,
                    Login = "Admin",
                    Password = "11111",
                    RoleId=0}
                });

            
            var rr = Mock.Of<IRepository<Role>>(
                r => r.GetAll() == new List<Role>() &&
            r.Get(0) == new Role { Id = 0, Name = "админ" });
            LoginViewModel lm = new LoginViewModel(ru, rr);
            lm.User.Login = "Admin";
            Assert.IsFalse(lm.Login("12345"));
        }
        [TestMethod()]
        public void Login_UserSign_Test()
        {
            var ru = Mock.Of<IRepository<User>>(
                r => r.GetAll() == new List<User>() {
                    new User {
                    Id = 1,
                    Login = "Admin",
                    Password = "11111",
                    RoleId=0}
                });
  
            //поддельный репозиторий ролей
            var rr = Mock.Of<IRepository<Role>>(
                r => r.GetAll() == new List<Role>() &&
            r.Get(0) == new Role { Id = 0, Name = "админ" });
            LoginViewModel lm = new LoginViewModel(ru, rr);
            lm.User.Login = "Admin";
            lm.Login("11111");
            Assert.AreEqual(ApplicationUser.Get().Login, "Admin");
            Assert.AreEqual(ApplicationUser.Get().Role, "админ");
        }
    }
}