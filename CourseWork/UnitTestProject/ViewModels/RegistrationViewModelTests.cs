﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CourseWork.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using DAL.Interfaces;
using DAL.Models;

namespace CourseWork.ViewModels.Tests
{
    [TestClass()]
    public class RegistrationViewModelTests
    {
        
        [TestMethod()]
        public void Registration_True_Test()
        {
            var ru = Mock.Of<IRepository<User>>(
                r => r.GetAll() == new List<User>() {
                    new User {
                    Id = 1,
                    Login = "Admin",
                    Password = "11111",
                    RoleId=0}
                });


            var rr = Mock.Of<IRepository<Role>>(
                r => r.GetAll() == new List<Role>()
                {
                    new Role { Id = 0, Name = "админ" },
                    new Role { Id = 0, Name = "Зритель" }
                } );
            RegistrationViewModel rm = new RegistrationViewModel(ru, rr);
            rm.Login = "Вася";
            rm.Password = "111";
            rm.Phone = "12345";
            rm.Address = "Гомель";

            Assert.IsTrue(rm.Registration());
        }
        [TestMethod()]
        public void Registration_False_Test()
        {
            var ru = Mock.Of<IRepository<User>>(
                r => r.GetAll() == new List<User>() {
                    new User {
                    Id = 1,
                    Login = "Admin",
                    Password = "11111",
                    RoleId=0} 
                });


            var rr = Mock.Of<IRepository<Role>>(
                r => r.GetAll() == new List<Role>()
                {
                    new Role { Id = 0, Name = "админ" },
                    new Role { Id = 0, Name = "Зритель" }
                });
            RegistrationViewModel rm = new RegistrationViewModel(ru, rr);
            rm.Login = "Admin";
            rm.Password = "111";
            rm.Phone = "12345";
            rm.Address = "Гомель";

            Assert.IsFalse(rm.Registration());
        }
        //[TestMethod()]
        //public void Registration_IsCorrectRegistration_Test()
        //{
        //    Mock<IRepository<User>> userR = new Mock<IRepository<User>>();
        //    userR.Setup(r=>r.Add(It.IsAny<User>())).Callback
        //    var ru = Mock.Of<IRepository<User>>(
        //        r => r.GetAll() == new List<User>() {
        //            new User {
        //            Id = 1,
        //            Login = "Admin",
        //            Password = "11111",
        //            RoleId=0}
        //        } );
            

        //    var rr = Mock.Of<IRepository<Role>>(
        //        r => r.GetAll() == new List<Role>()
        //        {
        //            new Role { Id = 0, Name = "админ" },
        //            new Role { Id = 0, Name = "Зритель" }
        //        });
        //    RegistrationViewModel rm = new RegistrationViewModel(ru, rr);
        //    rm.Login = "Вася";
        //    rm.Password = "111";
        //    rm.Phone = "12345";
        //    rm.Address = "Гомель";

        //    Assert.AreEqual("Вася", ru.GetAll().LastOrDefault().Login);
        //}
    }
}