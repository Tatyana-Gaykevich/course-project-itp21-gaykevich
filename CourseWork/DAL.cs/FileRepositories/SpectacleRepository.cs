﻿using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace DAL.FileRepositories
{
    public class SpectacleRepository : IRepository<Spectacle>
    {
        string fileName;
        XmlSerializer serializer;

        public SpectacleRepository(string fileName="Spectacles.xml")
        {
            this.fileName = fileName;
            serializer = new XmlSerializer(typeof(List<Spectacle>));
        }

        public void Add(Spectacle entity)
        {
            List<Spectacle> spectacles = new List<Spectacle>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        spectacles = (List<Spectacle>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (!spectacles.Any(s => entity.Id == s.Id))
            {
                spectacles.Add(entity);
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, spectacles);
                }
            }
        }

        public void Delete(Spectacle entity)
        {
            List<Spectacle> spectacles = new List<Spectacle>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        spectacles = (List<Spectacle>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (spectacles.Remove(spectacles.FirstOrDefault(u => u.Id == entity.Id)))
            {

                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, spectacles);
                }
            }
        }

        public Spectacle Get(int id)
        {
            List<Spectacle> spectacles = new List<Spectacle>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        spectacles = (List<Spectacle>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return spectacles.FirstOrDefault(u => u.Id == id);
        }

        public List<Spectacle> GetAll()
        {
            List<Spectacle> spectacles = new List<Spectacle>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        spectacles = (List<Spectacle>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return spectacles;
        }

        public void Update(Spectacle entity)
        {
            List<Spectacle> spectacles = new List<Spectacle>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        spectacles = (List<Spectacle>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            int index = spectacles.FindIndex(u => u.Id == entity.Id);
            if (index >= 0)
            {
                spectacles[index] = entity;
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, spectacles);
                }
            }
        }
    }
}
