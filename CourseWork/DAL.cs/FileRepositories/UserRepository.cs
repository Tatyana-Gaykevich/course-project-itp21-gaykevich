﻿using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace DAL.FileRepositories
{
    public class UserRepository : IRepository<User>
    {
        string fileName;
        XmlSerializer serializer;

        public UserRepository(string fileName="Users.xml")
        {
            this.fileName = fileName;
            serializer = new XmlSerializer(typeof(List<User>));
        }

        public void Add(User entity)
        {
            List<User> users = new List<User>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        users = (List<User>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (!users.Any(u => entity.Id == u.Id))
            {
                users.Add(entity);
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, users);
                }
            }
        }

        public void Delete(User entity)
        {
            List<User> users = new List<User>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        users = (List<User>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (users.Remove(users.FirstOrDefault(u => u.Id == entity.Id)))
            {

                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, users);
                }
            }
        }

        public User Get(int id)
        {
            List<User> users = new List<User>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        users = (List<User>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return users.FirstOrDefault(u => u.Id == id);
        }

        public List<User> GetAll()
        {
            List<User> users = new List<User>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        users = (List<User>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return users;
        }

        public void Update(User entity)
        {
            List<User> users = new List<User>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        users = (List<User>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            int index = users.FindIndex(u => u.Id == entity.Id);
            if (index>=0)
            {
                users[index] = entity;
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, users);
                }
            }
        }
    }
}
