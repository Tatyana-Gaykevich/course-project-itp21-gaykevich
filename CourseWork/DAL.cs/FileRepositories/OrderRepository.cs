﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DAL.FileRepositories
{
    public class OrderRepository : IRepository<Order>
    {
        string fileName;
        XmlSerializer serializer;

        public OrderRepository(string fileName="Orders.xml")
        {
            this.fileName = fileName;
            serializer = new XmlSerializer(typeof(List<Order>));
        }

        public void Add(Order entity)
        {
            List<Order> orders = new List<Order>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        orders = (List<Order>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (!orders.Any(o => entity.Id == o.Id))
            {
                orders.Add(entity);
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, orders);
                }
            }
        }

        public void Delete(Order entity)
        {
            List<Order> orders = new List<Order>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        orders = (List<Order>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (orders.Remove(orders.FirstOrDefault(o => o.Id == entity.Id)))
            {

                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, orders);
                }
            }
        }

        public Order Get(int id)
        {
            List<Order> orders = new List<Order>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        orders = (List<Order>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return orders.FirstOrDefault(o => o.Id == id);
        }

        public List<Order> GetAll()
        {
            List<Order> orders = new List<Order>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        orders = (List<Order>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return orders;
        }

        public void Update(Order entity)
        {
            List<Order> orders = new List<Order>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        orders = (List<Order>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            Order order = orders.FirstOrDefault(r => r.Id == entity.Id);
            if (order != null)
            {
                order.UserId = entity.UserId;
                order.CategoryId = entity.CategoryId;
                order.TicketCount = entity.TicketCount;
                order.SpectacleId = entity.SpectacleId;
                order.Status = entity.Status;
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, orders);
                }
            }
        }
    }
}
