﻿using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace DAL.FileRepositories
{
    public class RoleRepository : IRepository<Role>
    {
        string fileName;
        XmlSerializer serializer;
        public RoleRepository(string fileName="Roles.xml")
        {
            this.fileName = fileName;
            serializer = new XmlSerializer(typeof(List<Role>));
        }

        public void Add(Role entity)
        {
            List<Role> roles=new List<Role>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        roles = (List<Role>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (!roles.Any(r => entity.Id == r.Id))
            {
                roles.Add(entity);
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, roles);
                }
            }
        }

        public void Delete(Role entity)
        {
            List<Role> roles = new List<Role>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        roles = (List<Role>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (roles.Remove(roles.FirstOrDefault(r=>r.Id==entity.Id)))
            {

                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, roles);
                }
            }
        }

        public Role Get(int id)
        {
            List<Role> roles = new List<Role>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        roles = (List<Role>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return roles.FirstOrDefault(r => r.Id == id);
        }

        public List<Role> GetAll()
        {
            List<Role> roles = new List<Role>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        roles = (List<Role>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return roles;
        }

        public void Update(Role entity)
        {
            List<Role> roles = new List<Role>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        roles = (List<Role>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            Role role = roles.FirstOrDefault(r => r.Id == entity.Id);
            if (role != null)
            {
                role.Name = entity.Name;
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, roles);
                }
            }

        }
    }
}
