﻿using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace DAL.FileRepositories
{
    public class SoldTicketsRepository : IRepository<SoldTickets>
    {
        string fileName;
        XmlSerializer serializer;

        public SoldTicketsRepository(string fileName = "SoldTickets.xml")
        {
            this.fileName = fileName;
            serializer = new XmlSerializer(typeof(List<SoldTickets>));
        }

        public void Add(SoldTickets entity)
        {
            List<SoldTickets> soldTickets = new List<SoldTickets>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        soldTickets = (List<SoldTickets>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (!soldTickets.Any(st => entity.Id == st.Id))
            {
                soldTickets.Add(entity);
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, soldTickets);
                }
            }
        }

        public void Delete(SoldTickets entity)
        {
            List<SoldTickets> soldTickets = new List<SoldTickets>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        soldTickets = (List<SoldTickets>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (soldTickets.Remove(soldTickets.FirstOrDefault(st => st.Id == entity.Id)))
            {

                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, soldTickets);
                }
            }
        }

        public SoldTickets Get(int id)
        {
            List<SoldTickets> soldTickets = new List<SoldTickets>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        soldTickets = (List<SoldTickets>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return soldTickets.FirstOrDefault(st => st.Id == id);
        }

        public List<SoldTickets> GetAll()
        {
            List<SoldTickets> soldTickets = new List<SoldTickets>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        soldTickets = (List<SoldTickets>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return soldTickets;
        }

        public void Update(SoldTickets entity)
        {
            List<SoldTickets> soldTickets = new List<SoldTickets>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        soldTickets = (List<SoldTickets>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }

            int index = soldTickets.FindIndex(st => st.Id == entity.Id);

            if (index >= 0)
            {
                soldTickets[index] = entity;
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, soldTickets);
                }
            }

        }
    }
}
