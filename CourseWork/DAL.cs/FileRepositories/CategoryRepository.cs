﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DAL.FileRepositories
{
    public class CategoryRepository : IRepository<Category>
    {
        string fileName;
        XmlSerializer serializer;

        public CategoryRepository(string fileName = "Categories.xml")
        {
            this.fileName = fileName;
            serializer = new XmlSerializer(typeof(List<Category>));
        }


        public void Add(Category entity)
        {
            List<Category> categories = new List<Category>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        categories = (List<Category>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (!categories.Any(c => entity.Id == c.Id))
            {
                categories.Add(entity);
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, categories);
                }
            }
        }

        public void Delete(Category entity)
        {
            List<Category> categories = new List<Category>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        categories = (List<Category>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            if (categories.Remove(categories.FirstOrDefault(c => c.Id == entity.Id)))
            {

                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, categories);
                }
            }
        }

        public Category Get(int id)
        {
            List<Category> categories = new List<Category>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        categories = (List<Category>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return categories.FirstOrDefault(c => c.Id == id);
        }

        public List<Category> GetAll()
        {
            List<Category> categories = new List<Category>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        categories = (List<Category>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            return categories;
        }

        public void Update(Category entity)
        {
            List<Category> categories = new List<Category>();
            try
            {
                using (FileStream fs = File.OpenRead(fileName))
                {
                    if (fs.Length != 0)
                    {
                        categories = (List<Category>)serializer.Deserialize(fs);
                    }
                }
            }
            catch { }
            Category category = categories.FirstOrDefault(c => c.Id == entity.Id);
            

            int index = categories.FindIndex(c => c.Id == entity.Id);
            if (index >= 0)
            {
                categories[index] = entity;
                using (FileStream fs = File.Create(fileName))
                {
                    serializer.Serialize(fs, categories);
                }
            }
        }
    }
}

