﻿namespace DAL.Models
{

    //Характеристика заказанных билетов
    public class Order
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public int TicketCount { get; set; }
        public int SpectacleId { get; set; }
        public string Status { get; set; } // Доставлен билет или нет
    }
}
