﻿using System;

namespace DAL.Models
{
    //Характеристика спектаклей
    public class Spectacle
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Genre { get; set; } //Жанр спектакля   
        public DateTime Date { get; set; }
    }
}
