﻿namespace DAL.Models
{
    public class SoldTickets
    {
        public int Id { get; set; }
        public int SpectacleID { get; set; }
        public int CategoryId { get; set; }
        public int Count { get; set; }
    }
}
