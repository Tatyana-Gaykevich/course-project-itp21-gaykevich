﻿namespace DAL.Models
{
    //Категории(роли) пользовтелей
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
