﻿using DAL.EF;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        TheatreContext db;
        public OrderRepository()
        {
            db = new TheatreContext();
        }

        public OrderRepository(TheatreContext db)
        {
            this.db = db;
        }
        public void Add(Order entity)
        {
            db.Orders.Add(entity);
        }

        public void Delete(Order entety)
        {
            db.Orders.Remove(entety);
            db.SaveChanges();
        }

        public Order Get(int id)
        {
            return db.Orders.Find(id);
        }

        public List<Order> GetAll() => db.Orders.Include("User").Include("Category").ToList();

        public void Update(Order entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
