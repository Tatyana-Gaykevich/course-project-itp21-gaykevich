﻿using DAL.EF;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        TheatreContext db;
        public UserRepository()
        {
            db = new TheatreContext();
        }

        public UserRepository(TheatreContext db)
        {
            this.db = db;
        }
        public void Add(User entity)
        {
            db.Users.Add(entity);
        }

        public void Delete(User entety)
        {
            db.Users.Remove(entety);
            db.SaveChanges();
        }

        public User Get(int id)
        {
            return db.Users.Find(id);
        }

        public List<User> GetAll() => db.Users
            .Include("Role")
            .Include("Orders").ToList();

        public void Update(User entity)
        {
            db.Entry(entity).State =EntityState.Modified;
            db.SaveChanges();
        }
    }
}
