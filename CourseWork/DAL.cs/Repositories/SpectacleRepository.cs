﻿using DAL.EF;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class SpectacleRepository : IRepository<Spectacle>
    {
        TheatreContext db;
        public SpectacleRepository()
        {
            db = new TheatreContext();
        }

        public SpectacleRepository(TheatreContext db)
        {
            this.db = db;
        }
        public void Add(Spectacle entity)
        {
            db.Spectacles.Add(entity);
        }

        public void Delete(Spectacle entety)
        {
            db.Spectacles.Remove(entety);
            db.SaveChanges();
        }

        public Spectacle Get(int id)
        {
            return db.Spectacles.Find(id);
        }

        public List<Spectacle> GetAll() => db.Spectacles.Include("SoldTickets").ToList();

        public void Update(Spectacle entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
