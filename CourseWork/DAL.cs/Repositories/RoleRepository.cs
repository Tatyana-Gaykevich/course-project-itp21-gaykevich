﻿using DAL.EF;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class RoleRepository : IRepository<Category>
    {
        TheatreContext db;
        public RoleRepository()
        {
            db = new TheatreContext();
        }

        public RoleRepository(TheatreContext db)
        {
            this.db = db;
        }
        public void Add(Category entity)
        {
            db.Roles.Add(entity);
        }

        public void Delete(Category entety)
        {
            db.Roles.Remove(entety);
            db.SaveChanges();
        }

        public Category Get(int id)
        {
            return db.Roles.Find(id);
        }

        public List<Category> GetAll() => db.Roles.Include("Users").ToList();

        public void Update(Category entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
