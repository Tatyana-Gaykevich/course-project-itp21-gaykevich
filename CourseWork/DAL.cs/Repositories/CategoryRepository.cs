﻿using DAL.EF;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {
        TheatreContext db;
        public CategoryRepository()
        {
            db = new TheatreContext();
        }

        public CategoryRepository(TheatreContext db)
        {
            this.db = db;
        }
        public void Add(Category entity)
        {
            db.Categories.Add(entity);
        }

        public void Delete(Category entety)
        {
            db.Categories.Remove(entety);
            db.SaveChanges();
        }

        public Category Get(int id)
        {
            return db.Categories.Find(id);
        }

        public List<Category> GetAll() => db.Categories.Include("SoldTickets").Include("Orders").ToList();

        public void Update(Category entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
