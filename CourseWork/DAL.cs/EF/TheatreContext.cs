﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DAL.Models;

namespace DAL.EF
{
    public class TheatreContext: DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Category> Roles { get; set; }
        public DbSet<Spectacle> Spectacles { get; set; }
        public DbSet <User> Users { get; set; }
    }
}
