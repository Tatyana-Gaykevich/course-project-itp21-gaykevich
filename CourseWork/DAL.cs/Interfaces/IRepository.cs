﻿using System.Collections.Generic;

namespace DAL.Interfaces
{
    public interface IRepository<T>
    {
        List<T> GetAll();
        //CRUD

        void Add(T entity); //Create
        T Get (int id); //Read
        void Update(T entity);
        void Delete(T entety);
    }
}
