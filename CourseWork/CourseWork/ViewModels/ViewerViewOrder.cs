﻿using System;

namespace CourseWork.ViewModels
{
    public  class ViewerViewOrder
    {
        public int Id { get; set; }
        
        public string TitleSpectacle { get; set; }
        public string Genre { get; set; }
        public DateTime Date { get; set; }          
        public String Category { get; set; }
        public int TicketCount { get; set; }
        public double Cost { get; set; }
        public string Status { get; set; } // Доставлен билет или нет    
    }
}
