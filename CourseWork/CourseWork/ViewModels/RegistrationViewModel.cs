﻿using CourseWork.Commands;
using DAL.Interfaces;
using DAL.Models;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace CourseWork.ViewModels
{
    public class RegistrationViewModel : INotifyPropertyChanged
    {
        private readonly ICommand registrationCommand;
        private string login;
        private string password;
        private string address;
        private string phone;

        IRepository<User> repUser;
        IRepository<Role> repRole;

        public RegistrationViewModel(IRepository<User> repUser, IRepository<Role> repRole)
        {
            this.repUser = repUser;
            this.repRole = repRole;
            registrationCommand = new RegistationCommand(this);
        }

        public ICommand RegistrationCommand => registrationCommand;
        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                OnPropertyChanged();
            }
        }
        public string Password
        {
            get
            {return password;}
            set
            {
                password = value;
                OnPropertyChanged();
            }
        }
        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                OnPropertyChanged();
            }
        }
        public string Phone
        {
            get { return phone; }
            set
            {
                phone = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        //Метод для регистрации пользователей
        public bool Registration()
        {
            //UserRepository userRep = new UserRepository();
            User user = repUser.GetAll()
                .FirstOrDefault(u => u.Login == Login);
            if (user != null) return false; //такой пользователь уже есть
            Role role = repRole
                .GetAll()
                .FirstOrDefault(r => r.Name == "Зритель");
            
            user = new User
            {
                Id = repUser.GetAll().LastOrDefault().Id+1,
                Login = login,
                Password = password,
                Address = address,
                Phone = phone,
                RoleId = role.Id
            };
            repUser.Add(user);
            return true;
        }
    }
}
