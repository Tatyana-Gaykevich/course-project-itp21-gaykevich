﻿namespace CourseWork.ViewModels
{
    public  class StatSpectacle
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }
    }
}
