﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace CourseWork.ViewModels
{
    class GuestViewModel : INotifyPropertyChanged
    {
        private List<Spectacle> spectacles;
        IRepository <Spectacle> rep;
        private Spectacle selectedSpectacle; // Выбранный спектакль
        public event PropertyChangedEventHandler PropertyChanged;

        public GuestViewModel(IRepository<Spectacle> rep)
        {
            this.rep = rep;
            spectacles = rep.GetAll();
            selectedSpectacle = spectacles.FirstOrDefault(s => s.Date.Date == DateTime.Now.Date);
        }
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public List<Spectacle> Spectacles
        {
            get => spectacles;
            set
            {
                spectacles = value;
            }
        }

        public Spectacle SelectedSpectacle
        {
            get => selectedSpectacle;
            set
            {
                selectedSpectacle = value;
                OnPropertyChanged();
            }
        }

        public void ChangeSelectedSpectacle(object o, SelectionChangedEventArgs args)
        {
            Calendar c = (Calendar)o;
            SelectedSpectacle = spectacles.FirstOrDefault(s => s.Date.Date == c.SelectedDate?.Date);
        }

    }
}
