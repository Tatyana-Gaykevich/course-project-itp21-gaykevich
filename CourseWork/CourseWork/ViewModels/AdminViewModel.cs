﻿using CourseWork.Commands;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace CourseWork.ViewModels
{
    //Модель для представления  "Окно администратора"
    public class AdminViewModel : INotifyPropertyChanged
    {
        IRepository<User> repUser;
        IRepository<Order> repOrder;
        IRepository<Category> repCat;
        IRepository<SoldTickets> repSold;
        IRepository<Spectacle> repSpect;

        private readonly ICommand saveSpectaclesCommand;
        private readonly ICommand saveUsersCommand;
        private readonly ICommand saveCategoriesCommand;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public AdminViewModel(IRepository<User> repUser, IRepository<Order> repOrder, IRepository<Category> repCat, IRepository<SoldTickets> repSold, IRepository<Spectacle> repSpect)
        {
            this.repUser = repUser;
            this.repOrder = repOrder;
            this.repCat = repCat;
            this.repSold = repSold;
            this.repSpect = repSpect;

            
            saveUsersCommand = new SaveUsersCommand(this);
            saveCategoriesCommand = new SaveCategoriesCommand(this);
            saveSpectaclesCommand = new SaveSpectaclesCommand(this);

            spectacles = repSpect.GetAll();
            users = repUser.GetAll();
            ctgrs = repCat.GetAll();

            categories = InitStatCategories();
            statSpectacles = InitStatSpectacles();
        }

        private List<StatCategory> InitStatCategories()
        {
            List<StatCategory> result=new List<StatCategory>();
            foreach (var item in repCat.GetAll())
            {
                int count = repSold.GetAll().Where(s => s.CategoryId == item.Id).Sum(s=>s.Count);
                double cost = count * item.Price;
                result.Add(new StatCategory
                {
                    Name = item.Name,
                    Price = cost,
                    Count = count
                });
            }
            return result;
        }
        private List<StatSpectacle> InitStatSpectacles()
        {
            List<StatSpectacle> result = new List<StatSpectacle>();
            //получение названия всех спектаклей
            var spectacles = repSpect.GetAll().Select(s => s.Title).Distinct();
            foreach (var item in spectacles)
            {
                int sum = 0;
                double cost = 0;
                //получим все спектакли с таким названием
                var sp = repSpect.GetAll().Where(s => s.Title == item).ToList();
                foreach (var curSp in sp)
                {
                    int count = repSold.GetAll()
                        .Where(st => st.SpectacleID == curSp.Id)
                        .Sum(s => s.Count);//общее количество проданных билетов за один день
                    double curCost = repSold.GetAll()
                        .Where(st => st.SpectacleID == curSp.Id)
                        .Sum(s => repCat.Get(s.CategoryId).Price*s.Count);//общая стоимость проданных билетов за один день
                    sum += count;
                    cost += curCost;
                }

                
               
                result.Add(new StatSpectacle
                {
                    Name = item,
                    Price = cost,
                    Count = sum
                });
            }
            return result;
        }
        //для закладки "Спектакли"
        private List<Spectacle> spectacles;

        //для закладки "Пользователи"
        private List<User> users;

        //для закладки "Категории билетов"
        private List<Category> ctgrs;

        //для закладки "Отчетность"
        private List<StatCategory> categories;
        private List<StatSpectacle> statSpectacles;

        //свойства
        public ICommand SaveUsersCommand => saveUsersCommand;
        public ICommand SaveCategoriesCommand => saveCategoriesCommand;
        public ICommand SaveSpectaclesCommand => saveSpectaclesCommand;

        public List<Spectacle> Spectacles
        {
            get => spectacles;
            set
            {
                spectacles = value;
                OnPropertyChanged();
            }
        }
        public List<User> Users
        {
            get => users;
            set
            {
                users = value;
                OnPropertyChanged();
            }
            
        }
        public List<Category> Categories
        {
            get => ctgrs;
            set
            {
                ctgrs = value;
                OnPropertyChanged();
            }
        }
        public List<StatCategory> StatisticCategories
        {
            get => categories;
            set
            {
                categories = value;
            }
        }
        public List<StatSpectacle> StatisticSpectacles
        {
            get => statSpectacles;
            set
            {
                statSpectacles = value;
            }
        }
        public void SaveUsersChanges()
        {

            //удаление
            for (int i = 0; i < repUser.GetAll().Count; )
            {
                User cur = repUser.GetAll()[i];
                if (!Users.Any(u => u.Id == cur.Id))
                {
                    repUser.Delete(cur);
                    //Здесь нужно удалить все заказы этого пользователя
                    //При удалении заказа уменьшать количество проданных билетов, если заказ не доставлен!!!
                    
                }
                else
                    i++;
            }
            
            int count = repUser.GetAll().Count;
            for (int i = 0; i < count; i++)
            {
                repUser.Update(Users[i]);
            }
            if(Users.Count>count)
            {
                //id последнего пользователя
                int id = repUser.GetAll()[count - 1].Id;
                for (int i = count; i < Users.Count; i++)
                {
                    id++;
                    Users[i].Id = id;//!!!!
                    repUser.Add(Users[i]);
                    
                }
            }
            
        }
        public void SaveSpectaclesChanges()
        {
            for (int i = 0; i < repSpect.GetAll().Count;)
            {
                Spectacle spect = repSpect.GetAll()[i];
                if (!Spectacles.Any(c => c.Id == spect.Id))
                {
                    // если есть проданные билеты на спектакль, удалять нельзя
                    if (!repSold.GetAll().Any(s => s.SpectacleID == spect.Id && s.Count > 0))
                    {
                        //удаление всех продаж на этот спектакль !!!!!
                          
                        repSpect.Delete(spect);
                    }
                    else
                        Spectacles.Add(spect);
                }
                else
                    i++;
            }
            int count = repSpect.GetAll().Count;
            for (int i = 0; i < count; i++)
            {
                repSpect.Update(Spectacles[i]);
            }
            if (Spectacles.Count > count)
            {
                //id последней категории
                int id = repSpect.GetAll()[count - 1].Id;
                for (int i = count; i < Spectacles.Count; i++)
                {
                    id++;
                    Spectacles[i].Id = id;//!!!!
                    repSpect.Add(Spectacles[i]);

                }
            }
        }
        public void SaveCategoriesChanges()
        {
            //удаление
            for (int i = 0; i < repCat.GetAll().Count;)
            {
                Category cur = repCat.GetAll()[i];
                if (!Categories.Any(c => c.Id == cur.Id))
                {
                    repCat.Delete(cur);
                    // если есть  билеты этой категории в заказах , удалять нельзя
                    //удалить из SoldTicket
                }
                else
                    i++;
            }
            int count = repCat.GetAll().Count;
            for (int i = 0; i < count; i++)
            {
                repCat.Update(Categories[i]);
            }
            if (Categories.Count > count)
            {
                //id последней категории
                int id = repCat.GetAll()[count - 1].Id;
                for (int i = count; i <Categories.Count; i++)
                {
                    id++;
                    Categories[i].Id = id;//!!!!
                    repCat.Add(Categories[i]);

                }
            }
        }

    }

}
