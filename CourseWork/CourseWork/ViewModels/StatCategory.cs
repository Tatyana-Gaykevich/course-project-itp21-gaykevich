﻿namespace CourseWork.ViewModels
{
    public class StatCategory
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }
    }
}
