﻿using System;

namespace CourseWork.ViewModels
{
    public class CourierViewOrder
    {
        public int Id { get; set; }       
        public int TicketCount { get; set; }              
        public string Status { get; set; } // Доставлен билет или нет       
        public string UserName { get; set; }      
        public string TitleSpectacle { get; set; }     
        public DateTime Date { get; set; }            
        public String Category { get; set; }       
        public double Cost { get; set; }            
        public string Address { get; set;}           
        public string Phone { get; set; }             
    }
}
