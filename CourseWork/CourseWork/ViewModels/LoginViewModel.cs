﻿using CourseWork.Commands;
using CourseWork.Views;
using DAL.FileRepositories;
using DAL.Interfaces;
using DAL.Models;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;

namespace CourseWork.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        IRepository<User> repUser;
        IRepository<Role> repRole;

        private readonly ICommand okLoginCommand;
        private readonly ICommand showRegistrationCommand;
        private readonly ICommand okGuestCommand;

        public LoginViewModel(IRepository<User> repUser, IRepository<Role> repRole)
        {
            this.repUser = repUser;
            this.repRole = repRole;

            okLoginCommand = new OkLoginCommand(this);
            showRegistrationCommand = new ShowRegistrationCommand(this);
            okGuestCommand = new OkGuestCommand(this);

        }

        //Mетод открывает окно регистрации
        public void ShowRegistrationWindow()
        {
            RegistrationViewModel registrationViewModel = new RegistrationViewModel(repUser,repRole);
            RegistrationWindow registrationWindow = new RegistrationWindow();
            registrationWindow.DataContext = registrationViewModel;
            registrationWindow.ShowDialog();
        }

        //Mетод открывает окно для администратора
        public void ShowAdminWindow()
        {
            AdminViewModel adminViewModel = new AdminViewModel(
                new UserRepository(),
                new OrderRepository(),
                new CategoryRepository(),
                new SoldTicketsRepository(),
                new SpectacleRepository()
               );
            AdminWindow adminWindow = new AdminWindow();
            adminWindow.DataContext = adminViewModel;
            Window log = Application.Current.MainWindow;
            Application.Current.MainWindow = adminWindow;
            log.Close();
            adminWindow.ShowDialog();

        }

        //Mетод открывает окно для курьера
        public void ShowCurierWindow()
        {
            CourierViewModel courierViewModel = new CourierViewModel( 
                new UserRepository(),
                new OrderRepository(),
                new CategoryRepository(),                                              
                new SoldTicketsRepository(),
                new SpectacleRepository()
               );

            СourierWindow courierWindow = new СourierWindow();
            courierWindow.DataContext = courierViewModel;
            courierWindow.SelectOrder += courierViewModel.ChangeSelectedOrder;
            Window log = Application.Current.MainWindow;
            Application.Current.MainWindow = courierWindow;
            log.Close();
            courierWindow.ShowDialog();
        }

        //Mетод открывает окно для гостя
        public void ShowGuestWindow()
        {
            GuestViewModel guestViewModel = new GuestViewModel( new SpectacleRepository());
            GuestWindow guestWindow = new GuestWindow();
            guestWindow.DateChanged += guestViewModel.ChangeSelectedSpectacle;
            guestWindow.DataContext = guestViewModel;
            Window log = Application.Current.MainWindow;
            Application.Current.MainWindow = guestWindow;
            log.Close();
            guestWindow.ShowDialog();
        }

        //Mетод открывает окно для входа зарегистрированного пользователя
        public void ShowViewerWindow()
        {
            ViewerViewModel viewerViewModel = new ViewerViewModel(
                new SpectacleRepository(),
                new CategoryRepository(),
                new OrderRepository(),
                new SoldTicketsRepository(),
                new UserRepository()); 

            ViewerWindow viewerWindow = new ViewerWindow();
            viewerWindow.DateChanged += viewerViewModel.ChangeSelectedSpectacle;
            viewerWindow.SelectOrder += viewerViewModel.ChangeSelectOrder;
               
            viewerWindow.DataContext = viewerViewModel;
            Window log = Application.Current.MainWindow;
            Application.Current.MainWindow = viewerWindow;
            log.Close();
            viewerWindow.ShowDialog();
        }
        public ICommand OkLoginCommand => okLoginCommand;
        public ICommand ShowRegistrationCommand => showRegistrationCommand;
        public ICommand OkGuestCommand => okGuestCommand;
     

        public ApplicationUser User
        {
            get
            {
                return ApplicationUser.Get();
            }
            set
            {
                ApplicationUser.Get().Login = value.Login;
                ApplicationUser.Get().Role = value.Role;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        //Метод для аутентификации пользователя
        public bool Login(string password)
        {
            User user = repUser.GetAll()
                .FirstOrDefault(u => u.Password == password
                && u.Login == User.Login);
            if(user!=null)
                User.Role = repRole.Get(user.RoleId).Name;
            return (user != null);
        }

    }
}
