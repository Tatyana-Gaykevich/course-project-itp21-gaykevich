﻿using CourseWork.Commands;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Input;

namespace CourseWork.ViewModels
{
    //Модель для представления  "Окно курьера"
    public class CourierViewModel : INotifyPropertyChanged
    {
        IRepository<User> repUser;
        IRepository<Order> repOrder;
        IRepository<Category> repCat;
        IRepository<SoldTickets> repSold;
        IRepository<Spectacle> repSpect;

        private readonly ICommand checkOrderCommand;

        public void CheckOrder()
        {
            if (SelectedOrder != null && SelectedOrder.Status == "заказ")
            {
                Order checkOrder = repOrder.Get(SelectedOrder.Id);
                checkOrder.Status = "доставлен";
                repOrder.Update(checkOrder);
            }
        }
   
        private List<CourierViewOrder> courierOrders;
        private CourierViewOrder selectedOrder;

        public event PropertyChangedEventHandler PropertyChanged;

        public CourierViewModel(IRepository<User> repUser, IRepository<Order> repOrder, IRepository<Category> repCat, IRepository<SoldTickets> repSold, IRepository<Spectacle> repSpect)
        {
            this.repUser = repUser;
            this.repOrder = repOrder;
            this.repCat = repCat;
            this.repSold = repSold;
            this.repSpect = repSpect;


            courierOrders = InitOrders();
            checkOrderCommand = new CheckOrderCommand(this);
        }
        private List<CourierViewOrder> InitOrders()
        {
            
            
            List<Order> temp = repOrder.GetAll().ToList();
            List<CourierViewOrder> ord = new List<CourierViewOrder>();
            foreach (Order item in temp)
            {
                CourierViewOrder order = new CourierViewOrder();
                order.Id = item.Id;

                order.TitleSpectacle = repSpect.Get(item.SpectacleId).Title;
                order.TicketCount = item.TicketCount;
                Category cat = repCat.Get(item.CategoryId);
                order.Category = cat.Name;
                order.Status = item.Status;
                order.Cost = item.TicketCount * cat.Price;
                order.Date = repSpect.Get(item.SpectacleId).Date;
                order.UserName = repUser.Get(item.UserId).Login;
                order.Address = repUser.Get(item.UserId).Address;
                order.Phone = repUser.Get(item.UserId).Phone;
                ord.Add(order);
            }
            return ord;
        }

        internal void ChangeSelectedOrder(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dgr = (DataGrid)sender;
            SelectedOrder = courierOrders.FirstOrDefault(o => o.Id == ((CourierViewOrder)dgr.SelectedItem).Id);
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public ICommand CheckOrderCommand => checkOrderCommand;
        public List<CourierViewOrder> CourierOrders
        {
            get => courierOrders;
            set
            {
                courierOrders = value;
                OnPropertyChanged();
            }
        }

        //Выбранный заказ
        public CourierViewOrder SelectedOrder
        {
            get => selectedOrder;
            set
            {
                selectedOrder = value;
            }
        }
    }
}
