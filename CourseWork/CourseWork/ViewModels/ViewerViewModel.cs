﻿using CourseWork.Commands;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Input;

namespace CourseWork.ViewModels
{
    //Модель для представления ViwerWindow "Окно зарегистированного пользователя"
    public class ViewerViewModel : INotifyPropertyChanged
    {
        IRepository<User> repUser;
        IRepository<Order> repOrder;
        IRepository<Category> repCat;
        IRepository<SoldTickets> repSold;
        IRepository<Spectacle> repSpect;
        
        //для закладки "Афиша"
        private List<Spectacle> spectacles;
        
        private Spectacle selectedSpectacle; // Выбранный спектакль
       
        //для закладки "Мои заказы"
        private List<ViewerViewOrder> orders;
        private ViewerViewOrder selectedOrder;

        //для закладки "Поиск"
        private List<ViewerViewOrder>  findOrders;

        //Команды
        private readonly ICommand orderViewerCommand;
        private readonly ICommand orderCancelCommand;
        private readonly ICommand findOrderCommand;
        private readonly ICommand findDateCommand;
        private readonly ICommand findGenreCommand;
        private readonly ICommand findFreePlacesCommand;

        public event PropertyChangedEventHandler PropertyChanged;

        public ViewerViewModel(
           IRepository<Spectacle> repS, 
           IRepository<Category> repCat,
           IRepository<Order> repOrder,
           IRepository<SoldTickets> repSold,
           IRepository<User> repUser)
        {
            this.repSpect = repS;
            this.repCat = repCat;
            this.repOrder = repOrder;
            this.repSold = repSold;
            this.repUser = repUser;

            spectacles = repSpect.GetAll();
            selectedSpectacle = spectacles
                .FirstOrDefault(s => s.Date.Date == DateTime.Now.Date);

            orders= InitOrders();
            findOrders = InitOrders();
            orderViewerCommand = new OrderViewerCommand(this);
            orderCancelCommand = new OrderCancelCommand(this);

            findOrderCommand = new FindCommand(o =>
              {
                  {
                      try {
                          TextBox tb = (TextBox)o;
                          FindOrders = orders.Where(ord => ord.Category == tb.Text).ToList();
                      }
                      catch { }
                      
                  }
              });

            findDateCommand = new FindCommand(o =>
            {
                {
                    try
                    {
                        TextBox tb = (TextBox)o;
                        FindOrders = orders.Where(ord => ord.Date.Date == Convert.ToDateTime(tb.Text).Date).ToList();
                    }
                    catch { }

                }
            });
       
            findFreePlacesCommand = new FindCommand(o =>
            {
                {
                   

                    var sold = repSold.GetAll();
                    try
                    {       
                        Spectacles = spectacles.Where(s =>
                        {
                            var st = sold.Where(t => t.SpectacleID == s.Id).ToList();
                            return st.Any(t => t.Count < repCat.Get(t.CategoryId).Count);
                        }
                        ).ToList();
                    }
                    catch { }

                }
            });

            findGenreCommand = new FindCommand(o =>
            {
                {
                    try
                    {
                        TextBox tb = (TextBox)o;
                        FindOrders = orders.Where(ord => ord.Genre == tb.Text).ToList();
                    }
                    catch { }

                }
            });
            SelectedCategory = repCat.Get(1).Name;
        }

        public List<ViewerViewOrder> InitOrders()
        {
            int userId = repUser.GetAll().FirstOrDefault(u => u.Login == UserName).Id;
            List<Order> temp = repOrder.GetAll().Where(o => o.UserId == userId).ToList();
            List<ViewerViewOrder> ord = new List<ViewerViewOrder>();
            foreach (Order item in temp)
            {
                ViewerViewOrder order = new ViewerViewOrder();
                order.Id = item.Id;
                
                order.TitleSpectacle = repSpect.Get(item.SpectacleId).Title;
                order.TicketCount = item.TicketCount;
                Category cat = repCat.Get(item.CategoryId);
                order.Category = cat.Name;
                order.Status = item.Status;
                order.Cost = item.TicketCount * cat.Price;
                order.Date= repSpect.Get(item.SpectacleId).Date;
                order.Genre= repSpect.Get(item.SpectacleId).Genre;
                ord.Add(order);
            }
            return ord; 
        }

        

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public string UserName
        {
            get => ApplicationUser.Get().Login;          
        }
        public List<Spectacle> Spectacles
        {
            get => spectacles;
            set
            {
                spectacles = value;
                OnPropertyChanged();
            }
        }

        public Spectacle SelectedSpectacle
        {
            get => selectedSpectacle;
            set
            {
                selectedSpectacle = value;
                OnPropertyChanged();
            }
        }
        //Категории билетов
        public List<string> TicketCategories => repCat
            .GetAll()
            .Select(c => c.Name)
            .ToList();
        //Количество заказанных билетов
        public int TicketCount
        {
            get;set;
        }
        //Выбранная категория
        public string SelectedCategory
        {
            get; set;
        }
        public List<ViewerViewOrder> Orders
        {
            get => orders;
            set
            {
                orders = value;
                OnPropertyChanged();
            }
        }
        public List<ViewerViewOrder> FindOrders
        {
            get => findOrders;
            set
            {
                findOrders = value;
                OnPropertyChanged();
            }
        }
        //Выбранный заказ
        public ViewerViewOrder SelectedOrder
        {
            get => selectedOrder;
            set
            {
                selectedOrder = value;
                //OnPropertyChanged();
            }
        }

        internal void ChangeSelectedSpectacle(object sender, SelectionChangedEventArgs e)
        {
            Calendar c = (Calendar)sender;
            SelectedSpectacle = spectacles.
                FirstOrDefault(s => s.Date.Date == c.SelectedDate?.Date);
        }
        public void ChangeSelectOrder(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dgr = (DataGrid)sender;
            if(dgr.SelectedItem!=null)
            SelectedOrder = orders.FirstOrDefault(o => o.Id == ((ViewerViewOrder)dgr.SelectedItem).Id);
        }
        //свойство для ошибки
        public string error;
        public string Error
        {
            get => error;
            set
            {
                error = value;
                OnPropertyChanged();
            }
        }
        //Свойства для команд
        public ICommand OrderViewerCommand => orderViewerCommand;
        public ICommand OrderCancelCommand => orderCancelCommand;
        public ICommand FindOrderCommand => findOrderCommand;
        public ICommand FindDateCommand => findDateCommand;
        public ICommand FindGenreCommand => findGenreCommand;
        public ICommand FindFreePlacesCommand => findFreePlacesCommand;

        //Метод для заказа билетов
        public void Order()
        { 
            try 
            {
                if (SelectedSpectacle != null && TicketCount > 0)
                {
                    Category cat = repCat.GetAll()
                        .FirstOrDefault(c => c.Name == SelectedCategory);
                    SoldTickets sold = repSold.GetAll()
                         .Where(s => s.SpectacleID == SelectedSpectacle.Id)
                         .FirstOrDefault(s => s.CategoryId == cat.Id);

                    //Количество непроданных билетов выбранной категории
                    int countFreePlaces = cat.Count - sold.Count;
                    if (TicketCount <= countFreePlaces)
                    {
                        Order order = new Order();
                        int count = repOrder.GetAll().Count;
                        order.Id = repOrder.GetAll()[count - 1].Id + 1;//!!!!!!
                        
                        order.SpectacleId = SelectedSpectacle.Id;
                        order.TicketCount = TicketCount;
                        order.UserId = repUser
                            .GetAll()
                            .FirstOrDefault(u => u.Login == UserName)
                            .Id;
                        order.CategoryId = cat.Id;
                        order.Status = "заказ";
                        repOrder.Add(order);
                        sold.Count += TicketCount;
                        repSold.Update(sold);
                        Orders = InitOrders();
                        FindOrders = InitOrders();
                        Error = null;
                    }
                    else
                        Error = "Недостаточно свободных мест";
                }
                else
                {
                    Error = "Недостаточно свободных мест";
                }
            }
            catch {
                Error = "Что-то пошло не так";
            }

        }

        //Метод для отмены заказа билетов
        public void OrderCancel()
        {
            if(SelectedOrder!=null && SelectedOrder.Status=="заказ")
            {
                Order deleteOrder = repOrder
                    .GetAll()
                    .FirstOrDefault(o => o.Id == SelectedOrder.Id);


                // Возврат мест - уменьшение количества проданных билетов на этот спектакль
                SoldTickets st = repSold.GetAll().FirstOrDefault(s => s.SpectacleID == deleteOrder.SpectacleId && s.CategoryId == deleteOrder.CategoryId);
                    st.Count -= deleteOrder.TicketCount;
                 repSold.Update(st);
                 repOrder.Delete(deleteOrder);
               
                if (orders.Count > 0)
                    SelectedOrder = orders.FirstOrDefault();
                else
                    SelectedOrder = new ViewerViewOrder();
                Orders = InitOrders();
                FindOrders = InitOrders();

                Error = null;
            }
            else
            {
                Error = "Нельзя отменить выполненный заказ";
            }
        }
        
       
    }
}
