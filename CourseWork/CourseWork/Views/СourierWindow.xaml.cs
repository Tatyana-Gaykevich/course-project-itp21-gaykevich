﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CourseWork.Views
{
    /// <summary>
    /// Логика взаимодействия для СourierWindow.xaml
    /// </summary>
    public partial class СourierWindow : Window
    {
        public СourierWindow()
        {
            InitializeComponent();
        }

        public event SelectionChangedEventHandler SelectOrder
        {
            add
            {
                dataGridView2.SelectionChanged += value;
            }
            remove
            {
                dataGridView2.SelectionChanged -= value;
            }
        }
    }
}
