﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseWork.Views
{
    /// <summary>
    /// Логика взаимодействия для ViewerWindow.xaml
    /// </summary>
    public partial class ViewerWindow : Window
    {
        public ViewerWindow()
        {
            InitializeComponent();
            calendar1.DisplayDateStart = DateTime.Now;

        }
        public event EventHandler<SelectionChangedEventArgs> DateChanged
        {
            add
            {
                calendar1.SelectedDatesChanged += value;
            }
            remove
            {
                calendar1.SelectedDatesChanged -= value;
            }
        }
        public event SelectionChangedEventHandler SelectOrder
        {
            add
            {
                dataGridView2.SelectionChanged += value;
            }
            remove
            {
                dataGridView2.SelectionChanged -= value;
            }
        }
        //private void DataGridView1_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        //{
        //    PropertyDescriptor propertyDescriptor = (PropertyDescriptor)e.PropertyDescriptor;
        //    e.Column.Header = propertyDescriptor.DisplayName;
        //    if (propertyDescriptor.DisplayName == "Id")
        //    {
        //        e.Cancel = true;
        //    }
        //}

        //Заказать
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(titleLabel.Content!=null)
                orderGroupBox.Visibility = Visibility.Visible;
        }

        //Отмена
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            countTextBox.Text = "";
            orderGroupBox.Visibility = Visibility.Hidden;
        }

        private void Calendar1_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.Captured is CalendarItem)
                                Mouse.Capture(null);
        }

        private void Calendar1_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            orderGroupBox.Visibility = Visibility.Hidden;
        }

       
    }
}
