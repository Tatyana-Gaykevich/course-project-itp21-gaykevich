﻿using System;
using System.Windows.Input;

namespace CourseWork.Commands
{
    class FindCommand : ICommand
    {
        private Action<object> execute;

        public FindCommand(Action<object> execute)
        {
            this.execute = execute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;
        

        public void Execute(object parameter)
        {
            execute(parameter);
        }
    }
}
