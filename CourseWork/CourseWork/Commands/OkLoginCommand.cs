﻿using CourseWork.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CourseWork.Commands
{
    public class OkLoginCommand : ICommand
    {
        public event EventHandler CanExecuteChanged = delegate { };
        private LoginViewModel model;

        public OkLoginCommand(LoginViewModel model)
        {
            this.model = model;
        }

        public bool CanExecute(object parameter) => true;


        public void Execute(object parameter)
        {
            var passwordBox = parameter as PasswordBox;
            var password = passwordBox.Password;
            if (model.Login(password))
            {
                if (model.User.Role == "Зритель")
                    model.ShowViewerWindow();
                else
                   if (model.User.Role == "Курьер")
                        model.ShowCurierWindow();
                else if (model.User.Role == "Администратор")
                    model.ShowAdminWindow();
            }
            else
                MessageBox.Show("Проверьте правильность введенных данных");
        }
    }
}
