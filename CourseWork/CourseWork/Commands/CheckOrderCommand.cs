﻿using CourseWork.ViewModels;
using System;
using System.Windows.Input;

namespace CourseWork.Commands
{
    public class CheckOrderCommand : ICommand
    {
        CourierViewModel model;

        public CheckOrderCommand(CourierViewModel model)
        {
            this.model = model;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;
       

        public void Execute(object parameter)
        {
            model.CheckOrder(); 
        }
    }
}
