﻿using CourseWork.ViewModels;
using System;
using System.Windows.Input;

namespace CourseWork.Commands
{
    public class SaveUsersCommand : ICommand
    {
        AdminViewModel model;

        public SaveUsersCommand(AdminViewModel model)
        {
            this.model = model;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;
       

        public void Execute(object parameter)
        {
            model.SaveUsersChanges();
        }
    }
}
