﻿using CourseWork.ViewModels;
using System;
using System.Windows.Input;

namespace CourseWork.Commands
{
    public class OrderCancelCommand : ICommand
    {
        private ViewerViewModel model;

        public OrderCancelCommand(ViewerViewModel model)
        {
            this.model = model;
        }

        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            model.OrderCancel();
        }
    }
}
