﻿using CourseWork.ViewModels;
using System;
using System.Windows.Input;

namespace CourseWork.Commands
{
    class ShowRegistrationCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private LoginViewModel model;

        public ShowRegistrationCommand(LoginViewModel model)
        {
            this.model = model;
        }

        public bool CanExecute(object parameter) => true;


        public void Execute(object parameter)
        {
            model.ShowRegistrationWindow();
        }
    }
}
