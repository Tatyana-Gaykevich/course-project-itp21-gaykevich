﻿using CourseWork.ViewModels;
using System;
using System.Windows.Input;

namespace CourseWork.Commands
{
    public class OrderViewerCommand : ICommand
    {
        public event EventHandler CanExecuteChanged=delegate { };
        private ViewerViewModel model;

        public OrderViewerCommand(ViewerViewModel model)
        {
            this.model = model;
        }

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            model.Order();
        }
    }
}
