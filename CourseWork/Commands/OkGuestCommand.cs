﻿using CourseWork.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CourseWork.Commands
{
    class OkGuestCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private LoginViewModel model;

        public OkGuestCommand(LoginViewModel model)
        {
            this.model = model;
        }

        public bool CanExecute(object parameter) => true;


        public void Execute(object parameter)
        {
            model.ShowGuestWindow();
        }
      
    }
}
