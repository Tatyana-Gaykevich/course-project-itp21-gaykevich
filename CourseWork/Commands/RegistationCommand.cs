﻿using CourseWork.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CourseWork.Commands
{
    class RegistationCommand : ICommand

    {
        public event EventHandler CanExecuteChanged = delegate { };
        private RegistrationViewModel model;

        public RegistationCommand(RegistrationViewModel model)
        {
            this.model = model;
        }

        public bool CanExecute(object parameter) => true;
       

        public void Execute(object parameter)
        {
            if (model.Registration())
                MessageBox.Show("Регистрация прошла удачно " );
            else
                MessageBox.Show("Не получилось");
        }
    }
}
