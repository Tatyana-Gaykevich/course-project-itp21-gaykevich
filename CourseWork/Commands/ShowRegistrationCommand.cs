﻿using CourseWork.ViewModels;
using CourseWork;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System;

namespace CourseWork.Commands
{
    class ShowRegistrationCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private LoginViewModel model;

        public ShowRegistrationCommand(LoginViewModel model)
        {
            this.model = model;
        }

        public bool CanExecute(object parameter) => true;


        public void Execute(object parameter)
        {
            model.ShowRegistrationWindow();
        }
    }
}
