﻿using CourseWork.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CourseWork.Commands
{
    public class OkLoginCommand : ICommand
    {
        public event EventHandler CanExecuteChanged = delegate { };
        private LoginViewModel model;

        public OkLoginCommand(LoginViewModel model)
        {
            this.model = model;
        }

        public bool CanExecute(object parameter) => true;


        public void Execute(object parameter)
        {
            var passwordBox = parameter as PasswordBox;
            var password = passwordBox.Password;
            if (model.Login(password))
                MessageBox.Show("Удачно Вы " + ApplicationUser.Get().Login);
            else
                MessageBox.Show("Не получилось");

        }
    }
}
