﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace CourseWork.ViewModels
{
    //Текущий пользователь системы
    public class ApplicationUser
    {
        //Паттерн Singletone
        private static ApplicationUser user = new ApplicationUser();
        private ApplicationUser()
        {
        }
        public static ApplicationUser Get() => user;
        public string Login { get; set; }
        public string Role { get; set; }
    }
}
