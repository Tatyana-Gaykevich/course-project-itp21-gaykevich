﻿using CourseWork.Commands;
using CourseWork.ViewModels;
using CourseWork.Views;
using DAL.Models;
using DAL.FileRepositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CourseWork.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {

        private readonly ICommand okLoginCommand;
        private readonly ICommand showRegistrationCommand;
        private readonly ICommand okGuestCommand;

        public LoginViewModel()
        {
            okLoginCommand = new OkLoginCommand(this);
            showRegistrationCommand = new ShowRegistrationCommand(this);
            okGuestCommand = new OkGuestCommand(this);

        }
        //Mетод открывает окно регистрации
        internal void ShowRegistrationWindow()
        {
            RegistrationViewModel registrationViewModel = new RegistrationViewModel();
            RegistrationWindow registrationWindow = new RegistrationWindow();
            registrationWindow.DataContext = registrationViewModel;
            registrationWindow.ShowDialog();
        }

        //Mетод открывает окно для входа гостя
        internal void ShowGuestWindow()
        {
            GuestViewModel guestViewModel = new GuestViewModel( new SpectacleRepository());
            GuestWindow guestWindow = new GuestWindow();
            guestWindow.DateChanged += guestViewModel.ChangeSelectedSpectacle;
            guestWindow.DataContext = guestViewModel;
            guestWindow.ShowDialog();
        }

        public ICommand OkLoginCommand => okLoginCommand;
        public ICommand ShowRegistrationCommand => showRegistrationCommand;
        public ICommand OkGuestCommand => okGuestCommand;

        public ApplicationUser User
        {
            get
            {
                return ApplicationUser.Get();
            }
            set
            {
                ApplicationUser.Get().Login = value.Login;
                ApplicationUser.Get().Role = value.Role;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        //Метод для аутентификации пользователя
        public bool Login(string password)
        {
            UserRepository userRep = new UserRepository();
            User user = userRep.GetAll()
                .FirstOrDefault(u => u.Password == password
                && u.Login == User.Login);
            return (user != null);
        }

    }
}
