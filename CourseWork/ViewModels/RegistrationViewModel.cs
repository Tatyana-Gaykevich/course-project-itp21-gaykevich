﻿using DAL.Models;
using DAL.FileRepositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CourseWork.Commands;

namespace CourseWork.ViewModels
{
    public class RegistrationViewModel : INotifyPropertyChanged
    {
        private readonly ICommand registrationCommand;
        private string login;
        private string password;
        private string address;
        private string phone;
        public RegistrationViewModel()
        {
            registrationCommand = new RegistationCommand(this);
        }

        public ICommand RegistrationCommand => registrationCommand;
        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                OnPropertyChanged();
            }
        }
        public string Password
        {
            get
            {return password;}
            set
            {
                password = value;
                OnPropertyChanged();
            }
        }
        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                OnPropertyChanged();
            }
        }
        public string Phone
        {
            get { return phone; }
            set
            {
                phone = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        //Метод для регистрации пользователей
        public bool Registration()
        {
            UserRepository userRep = new UserRepository();
            User user = userRep.GetAll()
                .FirstOrDefault(u => u.Login == Login);
            if (user != null) return false; //такой пользователь уже есть
            Role role = (new RoleRepository())
                .GetAll()
                .FirstOrDefault(r => r.Name == "Зритель");
            Random rand = new Random();
            user = new User
            {
                Id = rand.Next(0, 1000),
                Login = login,
                Password = password,
                Address = address,
                Phone = phone,
                RoleId = role.Id
            };
            userRep.Add(user);
            return true;
        }
    }
}
